# $Id: Makefile,v 1.2 2004/06/15 08:17:56 msw Exp $

ifeq ($(KERNELRELEASE),)
KVER           := $(shell uname -r)
KDIR           := /lib/modules/$(KVER)/build
PWD            := $(shell pwd)
KMINOR         := $(shell echo $(KVER) | cut -d'.' -f2)
KOUT           := $(KDIR)

.PHONY: modules clean

modules:
ifeq ($(KDIR),$(KOUT))
	$(MAKE) -C $(KDIR) SUBDIRS=$(PWD) $@
else
	mkdir -p $(PWD)/tmp
	cp $(KOUT)/.config $(PWD)/tmp/
	$(MAKE) -C $(KDIR) SUBDIRS=$(PWD) O=$(PWD)/tmp/ $@
endif

clean:
	rm -f *.o *.ko *~ core* .dep* .*.d .*.cmd *.mod.c *.a *.s .*.flags .tmp_*

else
CONFIG_PRISM54=m
ifeq ($(PATCHLEVEL),4)
include Makefile.k24
endif # ifeq ($(PATCHLEVEL),4)
ifeq ($(PATCHLEVEL),6)
include $(PWD)/Makefile.k26
endif # ifeq ($(PATCHLEVEL),6)
endif # ifeq ($(KERNELRELEASE),)
