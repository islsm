/*
  GPLv2, or later.
*/
#ifndef _P54U_IOCTL_H
#define _P54U_IOCTL_H

#include <linux/netdevice.h>
#include <linux/wireless.h>

struct iw_statistics *
p54u_wireless_stats(struct net_device *ndev);

extern const struct iw_handler_def p54u_handler_def;

#endif /* _P54U_IOCTL_H */
