#include <linux/wireless.h>

#include "p54u_ioctl.h"
struct iw_statistics *
p54u_wireless_stats(struct net_device *ndev)
{
	islpci_private *priv = netdev_priv(ndev);

	return &priv->iwstatistics;
}
