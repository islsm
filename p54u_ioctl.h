/*
  GPLv2, or later.
*/
#include <linux/wireless.h>

struct iw_statistics *
p54u_wireless_stats(struct net_device *ndev);

