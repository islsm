#ifndef _HAVE_PROTOCOL_H
#define _HAVE_PROTOCOL_H

/* GPL */
/* jbnote */
#define P54U_NR_CHANNELS 14
#define P54U_CHANNEL_DEFS 8 
/* this seems to be present 
   in all kinds of frames 
   received on data and mgmt pipes */

struct p54u_rx {
	u16     length;
	u8      padding[14];
	void*   data;
	/* this can be p54u_mgmt_tx2 */ 
} __attribute__((packed));

struct p54u_tx 
{
	u32 magic1;
/* until i sort out what this means... */
#define P54U_TX_MAGIC1_CHANNEL     0x00020200
#define P54U_TX_MAGIC1_FILTER      0x00020200
#define P54U_TX_MAGIC1_80211       0x0002076c
	u16 length;
	u8  padding[10];
	void *data;
	/* this can be p54u_mgmt_tx */
}  __attribute__((packed));


/* outgoing mgmt readback frames */

/* len is the length of the _next_ (tx2) data sent */
struct p54u_mgmt_tx1 {
	u32     magic1;
#define P54U_TX1_MAGIC_1      0x02066c
	u16     len;
	u16     magic2;
#define P54U_TX1_MAGIC_2      0x2
	u32     padding1;
	u32     padding2;
} __attribute__ ((packed));

/* len is the length of the frame, including the
   sub_pos and sub_len fields below, which i believe 
   are part of the lower-level frame */
/* this comes as payload of p54u_tx or p54u_rx */
struct p54u_mgmt_tx2 {
	u16	magic1;
#define P54U_TX2_MAGIC_1      0x8000 
	u16	len;
	u32	magic2;
  //#define P54U_TX2_MAGIC_2_EVEN 0x81306510
  //#define P54U_TX2_MAGIC_2_ODD  0x812f1130
#define P54U_TX2_MAGIC_2_EVEN 0x81288290
#define P54U_TX2_MAGIC_2_ODD  0x81328030
  
        u32     magic3;
#define P54U_TX2_MAGIC_3      0xc
	u16	sub_pos;
	u16	sub_len;
	void*   data;
} __attribute__ ((packed));


/* incoming mgmt readback frame */
struct p54u_freq_struct1
{
	u16 freq; /* in MHz */
	u8 data[6];
} __attribute__ ((packed));

struct p54u_freq_substruct1
{
	u16 head;
	u8 tail;
} __attribute__ ((packed));

struct p54u_freq_struct2
{
	u16 freq;
	struct p54u_freq_substruct1 data[P54U_CHANNEL_DEFS];
} __attribute__ ((packed));

struct p54u_freq_struct3
{
	u16 freq;
	u8 data[8];
} __attribute__ ((packed));

/* this is payload for p54u_mgmt_tx2 */
struct p54u_mgmt_rx_header 
{
	u32 magic1;
	u16 padding;
	u16 preamble_length; /* this is null for linksys devices, but is not null for SMC dongles, for instance */
	void* data;
} __attribute__ ((packed));

struct p54u_mgmt_rx_frame 
{
	u8 unknown1[0x2c];
	u8 mac_addr[6];
	u8 unknown2[0x32];
	struct p54u_freq_struct1 l1[P54U_NR_CHANNELS];
	u8 unknown3[8];
	struct p54u_freq_struct2 l2[P54U_NR_CHANNELS];
	u8 unknown4[4];
	struct p54u_freq_struct3 l3[P54U_NR_CHANNELS];
	u8 unknown5[0x24];
	u16 crc;
} __attribute__ ((packed));


/*
 * protocol definitions, data frames
 */

/* this is payload of p54u_tx */
struct p54u_tx_control
{
	u16 magic1;
      /* response triggers a response on the management pipe */
#define P54U_TX_CONTROL_NORESPONSE  0x8001
#define P54U_TX_CONTROL_RESPONSE    0x8010
	u16 length;
	u32 magic2;
	u32 magic3;
	void *data;
} __attribute__ ((packed));


struct p54u_freq_substruct2
{
	u16 head;
	u8 subtails[4];
	u8 tail;
	u8 padding; /* zero */
} __attribute__ ((packed));

/* this comes as the payload of p54u_tx_control */
struct p54u_tx_control_channel
{
	u32 magic1;
#define P54U_TX_CONTROL_CHANNEL_MAGIC1_SCAN     0x00780002
#define P54U_TX_CONTROL_CHANNEL_MAGIC1_DEFAULT  0x00280006
	u8  padding1[20];
	struct p54u_freq_struct3 freq; /* directly from l3 */
	u16 unknown1; /* field unknown, the only one !*/
	u8  l1data[4]; /* filled with values from l1 */
	struct p54u_freq_substruct2 str[P54U_CHANNEL_DEFS]; /* filled with values from p54u_freq_struct1 from l2 */
	u32 padding; /* filled with 0 in old firmware, nothing in new */
} __attribute__ ((packed));

#define FULL_TX_CONTROL_CHANNEL sizeof(struct p54u_tx) - sizeof(void*) + sizeof(struct p54u_tx_control) - sizeof(void*) + sizeof(struct p54u_tx_control_channel)

#define FULL_TX_CONTROL_FILTER 0x44
/* this also comes as the payload of p54u_tx_control */
struct p54u_tx_control_filter
{
      u16 unknown1;
      u8  destination[ETH_ALEN];
      u8  source[ETH_ALEN];
      /* I can see the breakup but can't tell what it means */
      void *data;
} __attribute__ ((packed));


#endif /* _HAVE_PROTOCOL_H */
