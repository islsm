/*
 * Prism54 USB driver 
 
T:  Bus=01 Lev=01 Prnt=01 Port=03 Cnt=01 Dev#=  2 Spd=480 MxCh= 0
D:  Ver= 2.00 Cls=ff(vend.) Sub=00 Prot=00 MxPS=64 #Cfgs=  1
P:  Vendor=5041 ProdID=2234 Rev= 2.02
C:* #Ifs= 1 Cfg#= 1 Atr=80 MxPwr=500mA
I:  If#= 0 Alt= 0 #EPs=11 Cls=ff(vend.) Sub=00 Prot=00 Driver=(none)
E:  Ad=01(O) Atr=02(Bulk) MxPS= 512 Ivl=0ms
E:  Ad=81(I) Atr=02(Bulk) MxPS= 512 Ivl=0ms
E:  Ad=02(O) Atr=02(Bulk) MxPS= 512 Ivl=0ms
E:  Ad=82(I) Atr=02(Bulk) MxPS= 512 Ivl=0ms
E:  Ad=83(I) Atr=02(Bulk) MxPS= 512 Ivl=0ms
E:  Ad=84(I) Atr=02(Bulk) MxPS= 512 Ivl=0ms
E:  Ad=8d(I) Atr=02(Bulk) MxPS= 512 Ivl=0ms
E:  Ad=0d(O) Atr=02(Bulk) MxPS= 512 Ivl=0ms
E:  Ad=8e(I) Atr=02(Bulk) MxPS= 512 Ivl=0ms
E:  Ad=0e(O) Atr=02(Bulk) MxPS= 512 Ivl=0ms
E:  Ad=8f(I) Atr=03(Int.) MxPS=   4 Ivl=125us

 */
#include <linux/usb.h>
#include <linux/wireless.h>

#include "net2280.h"
#include "p54u_protocol.h"


//#undef CONFIG_USB_DEBUG
#define CONFIG_USB_DEBUG y

#ifdef CONFIG_USB_DEBUG
#define p54u_dbg(format, arg...) if(debug > 1) printk(format "\n" ,##arg)
#else
#define p54u_dbg(format, arg...)
#endif
#define p54u_info(format, arg...) if(debug) printk(KERN_INFO __FILE__ ": " format "\n" , ##arg)
#define p54u_warn(format, arg...) printk(KERN_WARN __FILE__ ": " format "\n" , ##arg)
#define p54u_err(format, arg...) printk(KERN_ERR __FILE__ ": " format "\n" ,##arg)

#define p54u_time(arg...) 0LL
//#define p54u_time(arg...) (get_jiffies_64(##arg) - p54u->time)

#define P54U_TX_TIMEOUT		(2*HZ)

#define P54U_REG_WSIZE		(sizeof(struct p54u_reg))
#define P54U_REG_RSIZE		(P54U_REG_WSIZE - (sizeof(u32)))

#define P54U_PORT_BRG		0x0010
#define P54U_PORT_BRG_CFG	0x0000
#define P54U_PORT_DEV		0x0800
#define P54U_PORT_DEV_CFG	0x0880
#define P54U_PORT_U16		0x0003
#define P54U_PORT_U32		0x000f

#define P54U_PORT_BRG_U32	(P54U_PORT_BRG | P54U_PORT_U32)
#define P54U_PORT_BRG_CFG_U32	(P54U_PORT_BRG_CFG | P54U_PORT_U32)
#define P54U_PORT_BRG_CFG_U16	(P54U_PORT_BRG_CFG | P54U_PORT_U16)

#define P54U_PORT_DEV_U32	(P54U_PORT_DEV | P54U_PORT_U32)
#define P54U_PORT_DEV_CFG_U32	(P54U_PORT_DEV_CFG | P54U_PORT_U32)
#define P54U_PORT_DEV_CFG_U16	(P54U_PORT_DEV_CFG | P54U_PORT_U16)

/* pci */
#define NET2280_BASE		0x10000000
#define NET2280_BASE2		0x20000000

/* gpio */
#define P54U_BRG_POWER_UP	(1 << GPIO0_DATA)
#define P54U_BRG_POWER_DOWN	(1 << GPIO1_DATA)

/* devinit */
#define NET2280_CLK_4Mhz	(15 << LOCAL_CLOCK_FREQUENCY)
#define NET2280_CLK_30Mhz	(2 << LOCAL_CLOCK_FREQUENCY)
#define NET2280_CLK_60Mhz	(1 << LOCAL_CLOCK_FREQUENCY)
#define NET2280_CLK_STOP	(0 << LOCAL_CLOCK_FREQUENCY)
#define NET2280_PCI_ENABLE	(1 << PCI_ENABLE)
#define NET2280_PCI_SOFT_RESET	(1 << PCI_SOFT_RESET)

/* enpoints */
#define NET2280_CLEAR_NAK_OUT_PACKETS_MODE	(1 << CLEAR_NAK_OUT_PACKETS_MODE)
#define NET2280_FIFO_FLUSH			(1 << FIFO_FLUSH)

/* irq */
#define NET2280_USB_INTERRUPT_ENABLE		(1 << USB_INTERRUPT_ENABLE)
#define NET2280_PCI_INTA_INTERRUPT		(1 << PCI_INTA_INTERRUPT)
#define NET2280_PCI_INTA_INTERRUPT_ENABLE	(1 << PCI_INTA_INTERRUPT_ENABLE)

/* registers */
#define NET2280_DEVINIT		0x00
#define NET2280_USBIRQENB1	0x24
#define NET2280_IRQSTAT1	0x2c
#define NET2280_FIFOCTL     0x38
#define NET2280_GPIOCTL		0x50
#define NET2280_RELNUM		0x88
#define NET2280_EPA_RSP		0x324
#define NET2280_EPA_STAT	0x32c
#define NET2280_EPB_STAT	0x34c
#define NET2280_EPC_RSP		0x364
#define NET2280_EPC_STAT	0x36c
#define NET2280_EPD_STAT	0x38c

#define NET2280_EPA_CFG     0x320
#define NET2280_EPB_CFG     0x340
#define NET2280_EPC_CFG     0x360
#define NET2280_EPD_CFG     0x380
#define NET2280_EPE_CFG     0x3A0
#define NET2280_EPF_CFG     0x3C0


#define P54U_DEV_BASE		0x40000000

#define P54U_TRDY_TIMEOUT	0x40
#define P54U_RETRY_TIMEOUTG	0x41

#define P54U_IMAGE_FILE		"isl3890usb"

//#define P54U_MAX_FRAME_SIZE	16384
#define P54U_MAX_FRAME_SIZE	4096
//#define P54U_QUEUE_LEN	16
#define P54U_QUEUE_LEN		2
#define P54U_FW_BLOCK		512
#define P54U_INIT_BLOCK		0x03fc
#define P54U_MGMT_MEM_SIZE      0x2000

#define p54u_brg_writel(dev, addr, val)		p54u_reg_rw(dev, 1, P54U_PIPE_BRG, P54U_PORT_BRG_U32, addr, val)
#define p54u_pcicfg_brg_writel(dev, addr, val)	p54u_reg_rw(dev, 1, P54U_PIPE_BRG, P54U_PORT_BRG_CFG_U32, addr, val)
#define p54u_pcicfg_brg_writew(dev, addr, val)	p54u_reg_rw(dev, 1, P54U_PIPE_BRG, P54U_PORT_BRG_CFG_U16, addr, (val & 0xffff))

#define p54u_dev_writel(dev, reg, val)		p54u_reg_rw(dev, 1, P54U_PIPE_DEV, P54U_PORT_DEV_U32, (P54U_DEV_BASE | reg), val)
#define p54u_pcicfg_dev_writel(dev, addr, val)	p54u_reg_rw(dev, 1, P54U_PIPE_DEV, P54U_PORT_DEV_CFG_U32, addr, val)
#define p54u_pcicfg_dev_writew(dev, addr, val)	p54u_reg_rw(dev, 1, P54U_PIPE_DEV, P54U_PORT_DEV_CFG_U16, addr, (val & 0xffff))

#define p54u_brg_readl(dev, addr)		p54u_reg_rw(dev, 0, P54U_PIPE_BRG, P54U_PORT_BRG_U32, addr, 0)
#define p54u_pcicfg_brg_readl(dev, addr)	p54u_reg_rw(dev, 0, P54U_PIPE_BRG, P54U_PORT_BRG_CFG_U32, addr, 0)
#define p54u_pcicfg_brg_readw(dev, addr)	(p54u_reg_rw(dev, 0, P54U_PIPE_BRG, P54U_PORT_BRG_CFG_U16, addr, 0) & 0xffff)

#define p54u_dev_readl(dev, reg)		p54u_reg_rw(dev, 0, P54U_PIPE_DEV, P54U_PORT_DEV_U32, (P54U_DEV_BASE | reg), 0)
#define p54u_pcicfg_dev_readl(dev, addr)	p54u_reg_rw(dev, 0, P54U_PIPE_DEV, P54U_PORT_DEV_CFG_U32, addr, 0)
#define p54u_pcicfg_dev_readw(dev, addr)	(p54u_reg_rw(dev, 0, P54U_PIPE_DEV, P54U_PORT_DEV_CFG_U16, addr, 0) & 0xffff)

enum p54u_pipe_addr {
        P54U_PIPE_DATA =	0x01,
        P54U_PIPE_MGMT =	0x02,
	P54U_PIPE_3 =		0x03,
        P54U_PIPE_4 =		0x04,
        P54U_PIPE_BRG =		0x0d,
        P54U_PIPE_DEV =		0x0e,
	P54U_PIPE_INT =		0x0f,
};

enum p54u_pipe_index {
	P54U_TX_DATA,
	P54U_TX_MGMT,
	P54U_TX_BRG,
	P54U_TX_DEV,
	P54U_RX_DATA,
	P54U_RX_MGMT,
	P54U_RX_3,
	P54U_RX_4,
	P54U_RX_BRG,
	P54U_RX_DEV,
	P54U_RX_INT,
	P54U_PIPES,
};

enum p54u_state {
	P54U_BOOT,
	P54U_RUN,
	P54U_SHUTDOWN,
};

struct p54u_reg {
	u16	port;
	u32	addr;
	u32	val;
} __attribute__ ((packed));

struct p54u_mgmt_tx {
	u32	magic1;
	u32	magic2;
	u32	magic3;
	u16	pos;
	u16	len;
} __attribute__ ((packed));


struct p54u_pipe_desc {
	u8		addr;	/* the address of the endpoint */
	u8		type;	/* the transfer type of the endpoint */
	size_t		p_size;	/* the size of the packet */
	void		(*callback)(struct urb *urb, struct pt_regs *regs);
};

struct p54u_pipe {
	struct urb	**urb;	/* the urb used to send data */
	void		**buf;	/* the buffer */
	size_t		*size;	/* the size of the buffer */
	int		len;	/* the length of the ringbufer */
	int		head;
	int		tail;
	
	atomic_t	busy;	/* true if an urb has been submitted */
	wait_queue_head_t wqh;
	struct completion comp; /* wait for the operation to finish */
	int done;
	
	u8		addr;	/* the address of the endpoint */
	u8		type;	/* the transfer type of the endpoint */
	size_t		pkt_size; /* the size of the packet */
};

/* Structure to hold all of our device specific stuff */
struct p54u {
	struct usb_device	*usbdev;	/* save off the usb device pointer */
	struct usb_interface	*interface;	/* save off the usb device pointer */
	struct net_device	*netdev;
	
	int			running;
	int                     state;
	int			err;
	
	struct workqueue_struct	*queue;
	struct work		*boot;
	struct completion	*comp;
	
	struct p54u_pipe	data_tx;
	struct p54u_pipe	data_rx;
	struct p54u_pipe	mgmt_tx;
	struct p54u_pipe	mgmt_rx;
	struct p54u_pipe	int_rx;
	
	struct semaphore	disconnect_sem;	/* prevent races between open() and disconnect() */
	
	const struct firmware	*fw_entry;
	u64 			time;
	u32			pending;
	
	/* added, not proper maybe */
	struct work_struct      int_ack;
  struct work_struct data_bh;
	struct p54u_mgmt_rx_frame mgmt_frame;
	struct net_device_stats statistics;
	struct iw_statistics iwstatistics;
};

extern int load_fw;
extern int debug;
extern const char dummy_mac[ETH_ALEN];

int p54u_setup_net(struct net_device *netdev);
int p54u_boot(struct net_device *netdev);
int p54u_shutdown(struct net_device *netdev);

void p54u_mdelay(int ms);
u32 p54u_reg_rw(struct net_device *netdev, int write, int ep, int port, u32 addr, u32 val);
int p54u_readi(struct net_device *netdev);

void p54u_int_rx_cb(struct urb *urb, struct pt_regs *p);
void p54u_int_ack(void *data);
void p54u_data_rx(void *data);

void p54u_data_rx_cb(struct urb *urb, struct pt_regs *p);
void p54u_mgmt_rx_cb(struct urb *urb, struct pt_regs *p);

int p54u_wait_int(struct net_device *netdev);
int p54u_wait_data(struct net_device *netdev);
int p54u_wait_mgmt_response(struct net_device *netdev);

int p54u_mgmt_rx_submit(struct net_device *netdev);
int p54u_data_rx_submit(struct net_device *netdev);

int p54u_bulk_msg(struct net_device *netdev, unsigned int ep, void *data, int len);
void p54u_data_debug(struct net_device *netdev, unsigned int ep, void *_data, int len);

int p54u_annouced_msg(struct net_device *netdev, unsigned int pipe, void *data, int data_len);

void make_tx_control_channel(struct p54u *p54u, void *buf, int chan);

#define p54u_control_msg(a,b,c) p54u_annouced_msg(a,P54U_PIPE_DATA,b,c)
#define p54u_mgmt_msg(a,b,c) p54u_annouced_msg(a,P54U_PIPE_MGMT,b,c)
