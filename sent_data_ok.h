// setup something
static char p54u_data_packet1[104] = 
  {0x00, 0x02, 0x02, 0x00, 0x56, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x01, 0x80, 0x4a, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00, 0x14, 0x0a, 0x06,
   0x02, 0x00, 0x1f, 0x00, 0xff, 0x03, 0x00, 0x00, 0x00, 0x00, 0x1f, 0x00, 0xff, 0x03, 0x00, 0x00,
   0x00, 0x00, 0x1f, 0x00, 0xff, 0x03, 0x00, 0x00, 0x00, 0x00, 0x1f, 0x00, 0xff, 0x03, 0x00, 0x00,
   0x00, 0x00, 0x1f, 0x00, 0xff, 0x03, 0x00, 0x00, 0x00, 0x00, 0x1f, 0x00, 0xff, 0x03, 0x00, 0x00,
   0x00, 0x00, 0x1f, 0x00, 0xff, 0x03, 0x00, 0x00, 0x00, 0x00, 0x1f, 0x00, 0xff, 0x03, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x06, 0x14 };


// send something : probe request ?
static char p54u_data_pkt_crc[0x44] = {
  0x00, 0x02, 0x02, 0x00, 0x34, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x01, 0x80, 0x28, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0c,
  0x41, 0xde, 0x30, 0x96, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x00, 0xde, 0x01, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0c, 0x48, 0x02, 0x00, 0x10, 0x06, 0x03, 0x00,
  0x00, 0x00, 0x3f, 0xc1 };


// send something
static char p54u_data_filter[0x44] = {
  0x00, 0x02, 0x02, 0x00, 0x34, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x01, 0x80, 0x28, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xde, 0x00, 0x00, 0x00, 0x00,
  0x08, 0x06, 0x04, 0x01, 0x00, 0x00, 0x00, 0x00, 0x0c, 0x48, 0x02, 0x00, 0x10, 0x06, 0x03, 0x00,
  0x00, 0x00, 0x00, 0x00 };

/* last packet is URB 825 */
