/*
 * Prism54 USB driver
 */

#include <linux/config.h>
#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/module.h>
#include <linux/smp_lock.h>
#include <linux/completion.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/usb.h>
#include <linux/pci.h>
#include <linux/firmware.h>
#include <asm/uaccess.h>
#include "prism54_usb.h"
#include "isl_38xx.h"
#include "sent_data_ok.h"
#include "modes.h"
#include "channels.h"

#ifdef CONFIG_USB_DEBUG
int debug = 2;
#else
int debug;
#endif

int load_fw;
static const char driver_name[] = "prism54_usb";

static const struct usb_device_id p54u_table[] = {
	{USB_DEVICE(0x5041, 0x2234)},	/* Linksys WUSB54G */
	{USB_DEVICE(0x5041, 0x2235)},	/* Linksys WUSB54G Portable */
	{USB_DEVICE(0x1915, 0x2234)},	/* Linksys WUSB54G OEM */
	{USB_DEVICE(0x1915, 0x2235)},	/* Linksys WUSB54G Portable OEM */
	{USB_DEVICE(0x0506, 0x0a11)},	/* 3COM 3CRWE254G72 */
	{USB_DEVICE(0x2001, 0x3701)},	/* DLink G120 */
	{USB_DEVICE(0x0cde, 0x0006)},	/* Medion 40900 */
	{USB_DEVICE(0x0846, 0x4200)},	/* Netgear WG121 */
	{USB_DEVICE(0x0846, 0x4210)},	/* Netgear WG121 the second ? */
	{USB_DEVICE(0x0846, 0x4220)},	/* Netgear WG111 */
	{USB_DEVICE(0x0707, 0xee06)},	/* SMC 2862W-G */
	{USB_DEVICE(0x124a, 0x4023)},   /* Shuttle PN15 (Airvast WM168g) */
	{}
};

static const struct p54u_pipe_desc p54u_pipes[] = {
	{ P54U_PIPE_DATA,		USB_ENDPOINT_XFER_BULK },
	{ P54U_PIPE_MGMT,		USB_ENDPOINT_XFER_BULK },
	{ P54U_PIPE_BRG,		USB_ENDPOINT_XFER_BULK },
	{ P54U_PIPE_DEV,		USB_ENDPOINT_XFER_BULK },
	{ P54U_PIPE_DATA | USB_DIR_IN,	USB_ENDPOINT_XFER_BULK },
	{ P54U_PIPE_MGMT | USB_DIR_IN,	USB_ENDPOINT_XFER_BULK },
	{ P54U_PIPE_3    | USB_DIR_IN,	USB_ENDPOINT_XFER_BULK },
	{ P54U_PIPE_4    | USB_DIR_IN,	USB_ENDPOINT_XFER_BULK },
	{ P54U_PIPE_BRG  | USB_DIR_IN,	USB_ENDPOINT_XFER_BULK },
	{ P54U_PIPE_DEV  | USB_DIR_IN,	USB_ENDPOINT_XFER_BULK },
	{ P54U_PIPE_INT  | USB_DIR_IN,	USB_ENDPOINT_XFER_INT  },
};


MODULE_DESCRIPTION("Prism54 USB Driver");
MODULE_AUTHOR("Feyd <feyd@seznam.cz>");
MODULE_LICENSE("GPL");
MODULE_DEVICE_TABLE(usb, p54u_table);
MODULE_PARM(debug, "i");
MODULE_PARM(load_fw, "i");
MODULE_PARM_DESC(debug, "Debug enabled or not");
MODULE_PARM_DESC(load_fw, "Load firmware on probe (1) or open (0)");

static int p54u_reset_usb(struct net_device *netdev)
{
	struct p54u *p54u = netdev_priv(netdev);
	struct usb_device *usbdev = p54u->usbdev;
	int err = 0;
	u32 reg;
	
//	return 0;

	p54u_info("%s: Reset USB device.\n", netdev->name);
	err = usb_reset_device(usbdev);
	if(err) {
		p54u_err("%s: Reset USB device failed.\n", netdev->name);
		return err;
	}
//	p54u_mdelay(200);
	p54u_info("%s: Reset USB device done.\n", netdev->name);
	
	return 0;
	
    do_err:
	p54u_err("%s: Reset USB device failed.\n", netdev->name);
	return err;
}

static void p54u_free_pipe(struct net_device *netdev, struct p54u_pipe *pipe)
{
	struct p54u *p54u = netdev_priv(netdev);
	struct usb_device *usbdev = p54u->usbdev;
	int i;

	p54u_info("Freeing pipe.\n");
	
	/* first unlink all submitted urbs
	   We have to do this before freeing 
	   resources that are accessed by the 
	   completion callbacks, such as completion 
	   variables */

	if(atomic_read(&pipe->busy)) {
		p54u_info("pipe->busy is set to %i",atomic_read(&pipe->busy));
		// this will do for the current situation
		usb_unlink_urb(pipe->urb[0]);
	}

	if(!pipe->buf) {
		p54u_info("pipe->buf == NULL\n");
		goto do_free_urb;
	}

	if(!pipe->urb) {
		p54u_info("pipe->urb == NULL\n");
		goto do_free_size;
	}

	if(!pipe->size) {
		p54u_info("pipe->size == NULL\n");
		goto do_clear;
	}
	
	for(i = 0; i < pipe->len; i++) {
		if(pipe->urb[i]) {
			if(pipe->buf[i]) {
				usb_buffer_free(usbdev, pipe->size[i], pipe->buf[i], pipe->urb[i]->transfer_dma);
			} else {
				p54u_info("pipe->buf[%i] == NULL\n", i);

			}
			usb_free_urb(pipe->urb[i]);
		} else {
			p54u_info("pipe->urb[%i] == NULL\n", i);
		}
	}

	kfree(pipe->buf);
	
    do_free_urb:
	if(pipe->urb) {
	    kfree(pipe->urb);
	} else {
		p54u_info("pipe->urb == NULL\n");
	}
	
    do_free_size:
    	if(pipe->size) {
		kfree(pipe->size);
	} else {
		p54u_info("pipe->size == NULL\n");
	}
	
    do_clear:
	p54u_info("Clear the memory.\n");

	memset(pipe, 0, sizeof(*pipe));
	
	p54u_info("Freeing pipe done.\n");
	
	return;
}

static void p54u_free_buffers(struct net_device *netdev)
{
	struct p54u *p54u = netdev_priv(netdev);
	struct usb_device *usbdev = p54u->usbdev;

	p54u_free_pipe(netdev, &p54u->data_tx);
	p54u_free_pipe(netdev, &p54u->data_rx);
	p54u_free_pipe(netdev, &p54u->mgmt_tx);
	p54u_free_pipe(netdev, &p54u->mgmt_rx);
	p54u_free_pipe(netdev, &p54u->int_rx);
	
	return;
}

static int p54u_reset_pipe(struct usb_device *usbdev, int addr)
{
  int err = 0;
  
  p54u_dbg("resetting pipe %02x", addr);
  switch(addr) {
  case P54U_PIPE_DATA:
  case P54U_PIPE_MGMT:
  case P54U_PIPE_BRG :
    err = usb_clear_halt(usbdev, usb_sndbulkpipe(usbdev,addr) );
    break;
  case P54U_PIPE_BRG | USB_DIR_IN :
  case P54U_PIPE_DATA | USB_DIR_IN:
  case P54U_PIPE_MGMT | USB_DIR_IN:
    err = usb_clear_halt(usbdev, usb_rcvbulkpipe(usbdev,addr) );
    break;
  case P54U_PIPE_INT | USB_DIR_IN:
    err = usb_clear_halt(usbdev, usb_rcvintpipe(usbdev,addr) );
    break;
  default:
    break;
  }
  return err;
}

static int p54u_reset_pipes(struct net_device *netdev)
{
	struct p54u *p54u = netdev_priv(netdev);
	struct usb_device *usbdev = p54u->usbdev;

	(void) p54u_reset_pipe(usbdev, P54U_PIPE_DEV | USB_DIR_IN );
	(void) p54u_reset_pipe(usbdev, P54U_PIPE_DEV);
	(void) p54u_reset_pipe(usbdev, P54U_PIPE_BRG | USB_DIR_IN );
	(void) p54u_reset_pipe(usbdev, P54U_PIPE_BRG);

	(void) p54u_reset_pipe(usbdev, P54U_PIPE_INT | USB_DIR_IN );

	(void) p54u_reset_pipe(usbdev, P54U_PIPE_DATA | USB_DIR_IN );
	(void) p54u_reset_pipe(usbdev, P54U_PIPE_DATA);
	(void) p54u_reset_pipe(usbdev, P54U_PIPE_MGMT | USB_DIR_IN );
	(void) p54u_reset_pipe(usbdev, P54U_PIPE_MGMT);

	return 0;
}

static int p54u_alloc_pipe(struct net_device *netdev, struct usb_endpoint_descriptor *desc, struct p54u_pipe *pipe, usb_complete_t callback)
{
	struct p54u *p54u = netdev_priv(netdev);
	struct usb_device *usbdev = p54u->usbdev;
	int err, i, p, e;
	
	if(!pipe)
		return 0;

	p54u_info("%s: Allocate pipe %02x, max pkt size is %02x\n", netdev->name, desc->bEndpointAddress, desc->wMaxPacketSize);
	
	pipe->pkt_size = desc->wMaxPacketSize;
	pipe->type = desc->bmAttributes & USB_ENDPOINT_XFERTYPE_MASK;
	pipe->addr = desc->bEndpointAddress;
	pipe->len = P54U_QUEUE_LEN;
	
	pipe->size = kmalloc(sizeof(*pipe->size) * pipe->len, GFP_KERNEL);
	if(!pipe->size) {
		p54u_info("Failed to allocate pipe->size\n");
		goto do_enomem;
	} else {
		p54u_info("pipe->size == %p\n", pipe->size);
	}
	memset(pipe->size, 0, sizeof(*pipe->size) * pipe->len);


	pipe->urb = kmalloc(sizeof(*pipe->urb) * pipe->len, GFP_KERNEL);
	if(!pipe->urb) {
		p54u_info("Failed to allocate pipe->urb\n");
		goto do_enomem;
	} else {
		p54u_info("pipe->urb == %p\n", pipe->urb);
	}
	memset(pipe->urb, 0, sizeof(*pipe->urb) * pipe->len);
	
	pipe->buf = kmalloc(sizeof(*pipe->buf) * pipe->len, GFP_KERNEL);
	if(!pipe->buf) {
		p54u_info("Failed to allocate pipe->buf\n");
		goto do_enomem;
	} else {
		p54u_info("pipe->buf == %p\n", pipe->buf);
	}
	memset(pipe->buf, 0, sizeof(*pipe->buf) * pipe->len);

	init_completion(&pipe->comp);
	atomic_set(&pipe->busy, 0);

	p54u_info("%s: Allocate buffers.\n", netdev->name);
	
	for(i = 0; i < pipe->len; i++) {
		pipe->size[i] = P54U_MAX_FRAME_SIZE;
		if(pipe->type == USB_ENDPOINT_XFER_INT) {
			pipe->size[i] = 4;
		}
		
		p54u_info("pipe->size [%i] = %i\n", i, pipe->size[i]);
		
		pipe->urb[i] = usb_alloc_urb(0, GFP_KERNEL);
		if(!pipe->urb[i]) {
			p54u_info("Failed to allocate pipe->urb[%i]\n", i);
			goto do_enomem;
		} else {
			p54u_info("pipe->urb[%i] == %p\n", i, pipe->urb[i]);
		}
		pipe->urb[i]->transfer_flags = (URB_NO_TRANSFER_DMA_MAP | URB_ASYNC_UNLINK);

		pipe->buf[i] = usb_buffer_alloc(usbdev, pipe->size[i], GFP_KERNEL, &pipe->urb[i]->transfer_dma);
		if(!pipe->buf[i]) {
			p54u_info("Failed to allocate pipe->buf[%i]\n", i);
			goto do_enomem;
		} else {
			p54u_info("pipe->buf[%i] == %p\n", i, pipe->buf[i]);
		}
		
		e = pipe->addr & USB_ENDPOINT_NUMBER_MASK;
		p54u_info("pipe->addr = %i\n", e);
//		e = pipe->addr;
		switch(pipe->type) {
		case USB_ENDPOINT_XFER_BULK:
			p = pipe->addr & USB_DIR_IN ? usb_rcvbulkpipe(usbdev, e) : usb_sndbulkpipe(usbdev, e);
			usb_fill_bulk_urb(pipe->urb[i], usbdev, p, pipe->buf[i], pipe->size[i], callback, netdev);
		case USB_ENDPOINT_XFER_INT:
			p = usb_rcvintpipe(usbdev, e);
			usb_fill_int_urb(pipe->urb[i], usbdev, p, pipe->buf[i], pipe->size[i], callback, netdev, 6);
//			usb_fill_int_urb(pipe->urb[i], usbdev, p, pipe->buf[i], pipe->size[i], callback, netdev, HZ / 4);
//			p = pipe->addr & USB_DIR_IN ? usb_rcvbulkpipe(usbdev, e) : usb_sndbulkpipe(usbdev, e);
//			usb_fill_bulk_urb(pipe->urb[i], usbdev, p, pipe->buf[i], pipe->size[i], callback, netdev);
		}
	}
	
	p54u_info("%s: Allocate pipe done.\n", netdev->name);
	return 0;
	
    do_enomem:
	p54u_info("%s: Not enough memory.\n", netdev->name);
	err = -ENOMEM;
    do_err:
	p54u_info("%s: Allocate pipe failed.\n", netdev->name);
	return err;
}

static int p54u_alloc_buffers(struct net_device *netdev)
{
	struct p54u *p54u = netdev_priv(netdev);
	struct usb_device *usbdev = p54u->usbdev;
	struct usb_interface *interface = p54u->interface;
	struct usb_host_interface *iface_desc = &interface->altsetting[0];
	struct usb_endpoint_descriptor *desc;
	struct p54u_pipe *pipe;
	int err = 0, i;
	
	p54u_info("%s: Setup USB structures.\n", netdev->name);
	
	for(i = 0; i < iface_desc->desc.bNumEndpoints; ++i) {
		desc = &iface_desc->endpoint[i].desc;
		switch(desc->bEndpointAddress) {
		case P54U_PIPE_DATA:
			err = p54u_alloc_pipe(netdev, desc, &p54u->data_tx, NULL);
			break;
		case P54U_PIPE_MGMT:
			err = p54u_alloc_pipe(netdev, desc, &p54u->mgmt_tx, NULL);
			break;
		case P54U_PIPE_DATA | USB_DIR_IN:
			err = p54u_alloc_pipe(netdev, desc, &p54u->data_rx, p54u_data_rx_cb);
			break;
		case P54U_PIPE_MGMT | USB_DIR_IN:
			err = p54u_alloc_pipe(netdev, desc, &p54u->mgmt_rx, p54u_mgmt_rx_cb);
			break;
		case P54U_PIPE_INT | USB_DIR_IN:
			err = p54u_alloc_pipe(netdev, desc , &p54u->int_rx, p54u_int_rx_cb);
			break;
		default:
			break;
		}
		if(err)
			goto do_err;
	}
	p54u_info("%s: Setup USB structures successful.\n", netdev->name);

	return 0;
	
    do_err:
    	p54u_info("%s: Setup USB structures failed: %i\n", netdev->name, err);
	p54u_free_buffers(netdev);
	
	return err;
}

static int p54u_reset_dev(struct net_device *netdev)
{
	struct p54u *p54u = netdev_priv(netdev);
	struct usb_device *usbdev = p54u->usbdev;
	u32 reg, revision;
	int err;
	
	p54u_info("%s: Reset device.\n", netdev->name);
	
	/* Bridge */
	/* Reset the usb<->pci bridge */
	p54u_dbg("%s: Reset bridge\n", netdev->name);
	reg = p54u_brg_readl(netdev, NET2280_GPIOCTL);
	//reg = 0x3e;
	//reg |= 0x3e;
	p54u_brg_writel(netdev, NET2280_GPIOCTL, (reg | P54U_BRG_POWER_DOWN) & ~P54U_BRG_POWER_UP);
	if(p54u->err)
		goto do_err;
	p54u_mdelay(100);
	p54u_brg_writel(netdev, NET2280_GPIOCTL, (reg | P54U_BRG_POWER_UP) & ~P54U_BRG_POWER_DOWN);
	if(p54u->err)
		goto do_err;
	p54u_dbg("%s: Reset bridge done\n", netdev->name);
	p54u_mdelay(100);
	
	/* See 11.5.1 on net2280 doc */
	p54u_dbg("%s: Magic 1\n", netdev->name);
	p54u_brg_writel(netdev, NET2280_DEVINIT, NET2280_CLK_30Mhz | NET2280_PCI_ENABLE | NET2280_PCI_SOFT_RESET);
	if(p54u->err)
		goto do_err;
	p54u_dbg("%s: Magic 1 done\n", netdev->name);
	p54u_mdelay(20);
	
	/* Enable mmio and busmaster on the bridge */
	p54u_dbg("%s: Setup bridge pci resources\n", netdev->name);
	p54u_pcicfg_brg_writew(netdev, PCI_COMMAND, (PCI_COMMAND_MEMORY | PCI_COMMAND_MASTER));
	
	/* Set base address 0 */
	p54u_pcicfg_brg_writel(netdev, PCI_BASE_ADDRESS_0, NET2280_BASE);

	/* Set PCI_STATUS_REC_MASTER_ABORT (why?) */
	reg = p54u_pcicfg_brg_readw(netdev, PCI_STATUS);
	p54u_pcicfg_brg_writew(netdev, PCI_STATUS, (reg | PCI_STATUS_REC_MASTER_ABORT));
	
	/* Read revision? */
	revision = p54u_brg_readl(netdev, NET2280_RELNUM);
	p54u_dbg("%s: Setup bridge pci resources done\n", netdev->name);
	
	/* Magic */
	p54u_dbg("%s: Magic 2\n", netdev->name);
	p54u_brg_writel(netdev, NET2280_EPA_RSP, NET2280_CLEAR_NAK_OUT_PACKETS_MODE);
	p54u_brg_writel(netdev, NET2280_EPC_RSP, NET2280_CLEAR_NAK_OUT_PACKETS_MODE);
	p54u_dbg("%s: Magic 2 done\n", netdev->name);
	
	/* Set base address 2 for bridge : where the endpoint fifos are */
	p54u_dbg("%s: Setup bridge base addr 2\n", netdev->name);
	p54u_pcicfg_brg_writel(netdev, PCI_BASE_ADDRESS_2, NET2280_BASE2);
	p54u_dbg("%s: Setup bridge base addr 2 done\n", netdev->name);

	/* Device */
	/* Enable mmio and busmaster on the device */
	p54u_dbg("%s: Setup device pci resources\n", netdev->name);
	p54u_pcicfg_dev_writew(netdev, PCI_COMMAND | 0x10000, (PCI_COMMAND_MEMORY | PCI_COMMAND_MASTER));
//	p54u_pcicfg_dev_writew(netdev, PCI_COMMAND, (PCI_COMMAND_MEMORY | PCI_COMMAND_MASTER));
	
	/* Set TRDY_TIMEOUT and RETRY_TIMEOUT to 0 */
	p54u_pcicfg_dev_writew(netdev, P54U_TRDY_TIMEOUT | 0x10000, 0);
//	p54u_pcicfg_dev_writew(netdev, P54U_TRDY_TIMEOUT, 0);
	
	/* Set base address 0 */
	p54u_pcicfg_dev_writel(netdev, PCI_BASE_ADDRESS_0 | 0x10000, P54U_DEV_BASE);
//	p54u_pcicfg_dev_writel(netdev, PCI_BASE_ADDRESS_0, P54U_DEV_BASE);
	p54u_dbg("%s: Setup device pci resources done\n", netdev->name);
	
	/* See 7.6.5.5 and chapter 9 of net2280 doc */
	p54u_dbg("%s: Magic 3\n", netdev->name);
	p54u_brg_writel(netdev, NET2280_USBIRQENB1, 0);
//	p54u_brg_writel(netdev, NET2280_USBIRQENB1, NET2280_PCI_INTA_INTERRUPT_ENABLE | NET2280_USB_INTERRUPT_ENABLE);
	p54u_brg_writel(netdev, NET2280_IRQSTAT1, NET2280_PCI_INTA_INTERRUPT);
	p54u_dbg("%s: Magic 3 done\n", netdev->name);
	
	/* Assert wakeup */
	p54u_dbg("%s: Assert device\n", netdev->name);
	p54u_dev_writel(netdev, ISL38XX_DEV_INT_REG,  ISL38XX_DEV_INT_WAKEUP);
	if(p54u->err)
		goto do_err;
	p54u_mdelay(20);
	
	/* (see old driver for INT_ABORT definition) */
	p54u_dev_writel(netdev, ISL38XX_DEV_INT_REG,  ISL38XX_DEV_INT_ABORT);
	if(p54u->err)
		goto do_err;
	p54u_dbg("%s: Assert device done\n", netdev->name);
	p54u_mdelay(20);
	
	/* Reset the device */
	p54u_dbg("%s: Reset device\n", netdev->name);

	/* clear the RAMBoot, the CLKRUN and the Reset bit */
	reg = p54u_dev_readl(netdev, ISL38XX_CTRL_STAT_REG);
//	reg &= ~(ISL38XX_CTRL_STAT_RESET | ISL38XX_CTRL_STAT_RAMBOOT);
	reg &= ~(ISL38XX_CTRL_STAT_RESET | ISL38XX_CTRL_STAT_RAMBOOT | ISL38XX_CTRL_STAT_CLKRUN);
	p54u_dev_writel(netdev, ISL38XX_CTRL_STAT_REG, reg);
	p54u_mdelay(20); /* WRITEIO_DELAY */

	/* set the Reset bit without reading the register */
	p54u_dev_writel(netdev, ISL38XX_CTRL_STAT_REG, reg | ISL38XX_CTRL_STAT_RESET);
	p54u_mdelay(20); /* WRITEIO_DELAY */
	/* clear the Reset bit */
	p54u_dev_writel(netdev, ISL38XX_CTRL_STAT_REG, reg);

	if(p54u->err)
		goto do_err;
	p54u_dbg("%s: Reset device done\n", netdev->name);
	/* wait for the device to reboot */
	p54u_mdelay(100);
	p54u_dbg("%s: Disable irq\n", netdev->name);
	p54u_dev_writel(netdev, ISL38XX_INT_EN_REG, 0);
//	p54u_dev_writel(netdev, ISL38XX_INT_EN_REG, 0x00004004);
	p54u_dbg("%s: Disable irq done\n", netdev->name);
	
//	p54u_mdelay(50);
	
	/* Ack the irqs */
	p54u_dbg("%s: Ack irq\n", netdev->name);
	reg = p54u_dev_readl(netdev, ISL38XX_INT_IDENT_REG);
	p54u_dev_writel(netdev, ISL38XX_INT_ACK_REG, reg);
	if(p54u->err)
		goto do_err;
	p54u_dbg("%s: Ack done\n", netdev->name);

	/* Magic */
	p54u_dbg("%s: Magic 4\n", netdev->name);
	p54u_brg_writel(netdev, NET2280_EPA_STAT, NET2280_FIFO_FLUSH);
	p54u_brg_writel(netdev, NET2280_EPB_STAT, NET2280_FIFO_FLUSH);
	p54u_brg_writel(netdev, NET2280_EPC_STAT, NET2280_FIFO_FLUSH);
	p54u_brg_writel(netdev, NET2280_EPD_STAT, NET2280_FIFO_FLUSH);
	/* why this again ? */
	p54u_brg_writel(netdev, NET2280_EPA_STAT, NET2280_FIFO_FLUSH);
	p54u_dbg("%s: Magic 4 done\n", netdev->name);

	p54u_info("%s: Reset device done.\n", netdev->name);
	
	return 0;
	
    do_err:
    	p54u_err("%s: Reset error: %i\n", netdev->name, p54u->err);
    	err = p54u->err;
	p54u->err = 0;
	return err;
}

static int p54u_load_firmware(struct net_device *netdev)
{
	struct p54u *p54u = netdev_priv(netdev);
	struct usb_device *usbdev = p54u->usbdev;
	unsigned int pipe;
	u32 reg;
	void *data;
	void *kmptr;
	size_t len,retlen;
	int err, i, j, k, l;
	u64 t, t1, t2;

	char *rcv;
	
	p54u_info("%s: Load firmware.\n", netdev->name);

	/* Firmware */
	
	pipe = usb_sndbulkpipe(usbdev, P54U_PIPE_DATA);
	data = p54u->fw_entry->data;
	i = p54u->fw_entry->size;
	j = 0;
	kmptr = kmalloc(P54U_FW_BLOCK, GFP_KERNEL);
	if ( !kmptr ) {
		p54u->err = -ENOMEM;
		goto do_err;
	}
	
	while(i > 0) {
		len = i > P54U_FW_BLOCK ? P54U_FW_BLOCK : i;
		memcpy(kmptr, data, len);
		t1 =  p54u_time();
		p54u->err = usb_bulk_msg(usbdev, pipe, kmptr, len, &retlen, 10*HZ);
		t2 =  p54u_time();
		t = t2 - t1;

		p54u_dbg("Actually written %i of %i", retlen, len);		
		p54u_data_debug(netdev, P54U_PIPE_DATA, data, len);

		if(p54u->err) {
			p54u_dbg("%s: Error writing firmware block: writen %i of %i.\n", netdev->name, j, p54u->fw_entry->size);
			goto do_release;
		}
		
		data += P54U_FW_BLOCK;
		i -= P54U_FW_BLOCK;
		
		/* Magic */
		p54u_dev_writel(netdev, ISL38XX_DIR_MEM_BASE_REG, 0xc0000f00);
		p54u_dev_writel(netdev, 0x1020, 0);
		p54u_dev_writel(netdev, 0x1020, 1);
		p54u_dev_writel(netdev, 0x1024, len);
		p54u_dev_writel(netdev, 0x1028, j | 0x00020000);
		p54u_dev_writel(netdev, 0x0060, 0x20000000);
		p54u_dev_writel(netdev, 0x0064, len/4);
		p54u_dev_writel(netdev, 0x0068, 4);
		reg = p54u_dev_readl(netdev, 0x102c);
		if(i > 0)
			p54u_brg_writel(netdev, NET2280_EPA_STAT, NET2280_FIFO_FLUSH);
		if(p54u->err) {
			p54u_dbg("%s: Error updating registers: writen %i of %i.\n", netdev->name, j, p54u->fw_entry->size);
			goto do_release;
		}
		j += P54U_FW_BLOCK;

	}
	
	reg = p54u_dev_readl(netdev, ISL38XX_CTRL_STAT_REG);
//	reg |= ISL38XX_CTRL_STAT_RAMBOOT | ISL38XX_CTRL_STAT_CLKRUN;
	reg |= ISL38XX_CTRL_STAT_RAMBOOT;
	p54u_dev_writel(netdev, ISL38XX_CTRL_STAT_REG, reg);
	p54u_dev_writel(netdev, ISL38XX_CTRL_STAT_REG, reg | ISL38XX_CTRL_STAT_RESET);
	p54u_dev_writel(netdev, ISL38XX_CTRL_STAT_REG, reg);
	
	if(p54u->err) {
		p54u_dbg("%s: Error latching reset: %i\n", netdev->name, p54u->err);
	}

	if ( kmptr ) {
		kfree(kmptr);
	}
	p54u_info("%s: Load firmware done.\n", netdev->name);
	return 0;
	
    do_release:
    do_err:
	if ( kmptr ) {
		kfree(kmptr);
	}
    	p54u_err("%s: Load error: %i\n", netdev->name, p54u->err);
    	err = p54u->err;
	p54u->err = 0;
	return err;
}

int p54u_setup_dev(struct net_device *netdev)
{
	struct p54u *p54u = netdev_priv(netdev);
	struct usb_device *usbdev = p54u->usbdev;
	u32 reg, rst = 0;
	int i, j, k, pending, err;

	p54u->err = usb_submit_urb(p54u->int_rx.urb[0], GFP_KERNEL);
	if(p54u->err) {
		p54u_dbg("%s: Error submit int 0: %i\n", netdev->name, p54u->err);
	} else {
		p54u_dbg("%s: Submit int 0 ok.\n", netdev->name);
	}

	p54u->err = p54u_data_rx_submit(netdev);
	p54u->err = p54u_mgmt_rx_submit(netdev);


	//p54u_dev_writel(netdev, ISL38XX_INT_EN_REG, 0x80004004);
	p54u_dev_writel(netdev, ISL38XX_INT_EN_REG, 0x00004004);
	p54u_brg_writel(netdev, NET2280_IRQSTAT1, NET2280_PCI_INTA_INTERRUPT);
	p54u_brg_writel(netdev, NET2280_USBIRQENB1, NET2280_PCI_INTA_INTERRUPT_ENABLE | NET2280_USB_INTERRUPT_ENABLE);
	p54u_dev_writel(netdev, ISL38XX_DEV_INT_REG, ISL38XX_DEV_INT_RESET);
	
	p54u_wait_int(netdev);
	return p54u->err;
}

int p54u_mgmt_readback(struct net_device *netdev)
{
	struct p54u *p54u = netdev_priv(netdev);
	struct usb_device *usbdev = p54u->usbdev;
	struct p54u_mgmt_tx1 *tx1;
	struct p54u_mgmt_tx2 *tx2;
	
	int i, j, len, err;

	memset(p54u->mgmt_tx.buf[0], 0, p54u->mgmt_tx.size[0]);
	memset(p54u->mgmt_tx.buf[1], 0, p54u->mgmt_tx.size[1]);
	
	tx1 = p54u->mgmt_tx.buf[0];
	tx2 = p54u->mgmt_tx.buf[1];
	
	tx1->magic1 = cpu_to_le32(0x02066c);
	tx1->magic2 = cpu_to_le16(0x2);
	tx1->padding1 = 0;
	tx1->padding2 = 0;

	tx2->magic1 = cpu_to_le16(0x8000);
	/* The low bits of magic2 varies, i dont really know where the
	   values comes from */
	//	tx2->magic3 = cpu_to_le32(0xc);
	tx2->magic3 = cpu_to_le32(0xc);

	i = P54U_MGMT_MEM_SIZE;
	j = 0;
	
	/* Each iteration will trigger a 0x82 pipe response */
	while (i > 0) {
	  /* those alternate between addresses of buffers... used by the windows driver, only. with ndiswrapper, this alternates between three values.
	   We can put here a value that will not be overwritten... and given bakc in the response packet */
	  tx2->magic2 = cpu_to_le32(0);
	  j++;
	  
	  len = i > P54U_INIT_BLOCK ? P54U_INIT_BLOCK : i;
	  
	  /* announces to the device what the full length 
	     of the next mgmt frame will be */
	  tx1->len = cpu_to_le16(len + sizeof(*tx2) - sizeof(void*));
	  p54u_bulk_msg(netdev, P54U_PIPE_MGMT, tx1, 16);
	  
	  /* The +4 is for the length + pos field in p54u_mgmt_tx struct */
	  tx2->len = cpu_to_le16(len + 4);
	  tx2->sub_len = cpu_to_le16(len);
	  tx2->sub_pos = cpu_to_le16(P54U_MGMT_MEM_SIZE - i);
	  
	  /* The following two writes can be done concurrently */
	  p54u_mgmt_msg(netdev, tx2, len + sizeof(*tx2) -sizeof(void*));
	  
	  p54u_wait_mgmt_response(netdev);
	  
	  if (p54u->err)
	    break;
	  
	  if (j==1) {
		  /* maybe check size ? Hum ? */
		  struct p54u_rx * rxd = p54u->mgmt_rx.urb[0]->transfer_buffer;
		  struct p54u_mgmt_tx2 *txd = (struct p54u_mgmt_tx2 *) &rxd->data;
		  struct p54u_mgmt_rx_header *mgmt_h = (struct p54u_mgmt_rx_header *) &txd->data;
		  struct p54u_mgmt_rx_frame *mgmt;
		  int frame_offset;
		  
		  p54u_info("Storing mgm frame");

		  frame_offset = le16_to_cpu(mgmt_h->preamble_length);
		  mgmt = (struct p54u_mgmt_rx_frame *) &(((u8 *)&(mgmt_h->data))[frame_offset]);
		  memcpy(&p54u->mgmt_frame, (char*) mgmt, sizeof(p54u->mgmt_frame));
		  memcpy(netdev->dev_addr, &mgmt->mac_addr, ETH_ALEN);
	  }

	  p54u_mgmt_rx_submit(netdev);

	  i -= P54U_INIT_BLOCK;
	}
	
	return p54u->err;
}

int p54u_readback_conf(struct net_device *netdev)
{
	struct p54u *p54u = netdev_priv(netdev);
	int a;
	
	a = p54u_brg_readl(netdev,NET2280_FIFOCTL);
	p54u_info("FIFOCTL : %08x",a);

	a = p54u_brg_readl(netdev,NET2280_EPA_CFG);
	p54u_info("EPA : %08x",a);

	a = p54u_brg_readl(netdev,NET2280_EPB_CFG);
	p54u_info("EPB : %08x",a);

	a = p54u_brg_readl(netdev,NET2280_EPC_CFG);
	p54u_info("EPC : %08x",a);

	a = p54u_brg_readl(netdev,NET2280_EPD_CFG);
	p54u_info("EPD : %08x",a);

	return p54u->err;
}

inline void make_rx_filter_packet(struct net_device *netdev, char* data)
{
	struct p54u_tx *tx = (struct p54u_tx *) data;
	struct p54u_tx_control *txc = (struct p54u_tx_control *) &tx->data;
	struct p54u_tx_control_filter *txcf = (struct p54u_tx_control_filter *) &txc->data; 
	unsigned int i ;

	memcpy(data,p54u_data_filter,FULL_TX_CONTROL_FILTER);
	printk("origin %p, int0 %p, int1 %p, int2 %p, destination %p", data, tx, txc, txcf, &txcf->destination[0]);
	
	/* fill with ff:ff:ff:ff:ff:ff address */
  	for( i = 0 ; i < ETH_ALEN ; i++)
  	   txcf->source[i] = 0xff;
 	/* my mac address  */
 	memcpy(&txcf->destination[0],netdev->dev_addr,ETH_ALEN);
	
	return;
}

static int p54u_set_filter(struct net_device *netdev)
{
	struct p54u *p54u = netdev_priv(netdev);
	char* filter_packet;

	filter_packet = kmalloc(FULL_TX_CONTROL_FILTER, GFP_KERNEL);
	if (!filter_packet)
	  return -ENOMEM;

	make_rx_filter_packet(netdev, filter_packet);

	p54u_control_msg(netdev, p54u_mode_send, sizeof(p54u_mode_send));
	p54u_control_msg(netdev, filter_packet, FULL_TX_CONTROL_FILTER);
	p54u_control_msg(netdev, p54u_mode_monitor, sizeof(p54u_mode_monitor));

	return p54u->err;
}

int p54u_data_send(struct net_device *netdev)
{
	struct p54u *p54u = netdev_priv(netdev);
	struct usb_device *usbdev = p54u->usbdev;
	char *channel_packet = 0;
	int i;

	channel_packet=kmalloc(FULL_TX_CONTROL_CHANNEL,GFP_KERNEL);
	if (!channel_packet)
		return -ENOMEM;

	/* submit initial data on data pipe */
  	p54u_control_msg(netdev, p54u_data_packet1, sizeof(p54u_data_packet1)); 
 	/* p54u_control_msg(netdev, p54u_mode_send, sizeof(p54u_mode_send)); */
/*      	//p54u_control_msg(netdev, p54u_data_pkt_crc, sizeof(p54u_data_pkt_crc)); */
/* 	p54u_control_msg(netdev, p54u_data_filter, sizeof(p54u_data_filter)); */
/*  	p54u_control_msg(netdev, p54u_mode_monitor, sizeof(p54u_mode_monitor)); */
	p54u_set_filter(netdev);
	
	/* cycle through the channels */
/* 	for ( i = 1 ; i <= P54U_NR_CHANNELS ; i++) */
/* 	{ */
/* 		make_tx_control_channel(p54u, channel_packet, i); */
/* 		p54u_control_msg(netdev, p54u_freqs[i-1], FULL_TX_CONTROL_CHANNEL); */
/* 		p54u_wait_data(netdev); */
/* 	} */
	
	return p54u->err;
}

int p54u_boot(struct net_device *netdev)
{
	struct p54u *p54u = netdev_priv(netdev);
	int err;
	
	p54u->state = P54U_BOOT;

	p54u_info("%s: Boot the device.\n", netdev->name);

	p54u_dbg("%s: Request firmware.\n", netdev->name);
	
	err = request_firmware(&p54u->fw_entry, P54U_IMAGE_FILE, netdev->class_dev.dev);
	if(err) {
		p54u_err("%s: Request firmware failed: %i\n", netdev->name, err);
		return err;
	}
		
	p54u_dbg("%s: Request firmware done: len = %i.\n", netdev->name, p54u->fw_entry->size);
	
	p54u->time = get_jiffies_64();

	INIT_WORK(&p54u->int_ack, p54u_int_ack, netdev);
	INIT_WORK(&p54u->data_bh, p54u_data_rx, netdev);

	/* will be overwritten in mgmt_readback */
	memcpy(netdev->dev_addr, dummy_mac, ETH_ALEN);

	err = p54u_reset_usb(netdev);
	
	if(!err)
		err = p54u_alloc_buffers(netdev);
	if(!err)
		err = p54u_reset_dev(netdev);
	if(!err)
		err = p54u_load_firmware(netdev);
	if(!err)
		err = p54u_setup_dev(netdev);
 	if(!err) 
		err = p54u_mgmt_readback(netdev);
	//if(!err)
	//	err = p54u_readback_conf(netdev);
	/* dont do this yet, first generate properly channel freq change paquets */
	
	if(!err) 
		err = p54u_data_send(netdev);

	//	if(!err)
	//	  err = p54u_reset_pipes(netdev);

/* 	err = p54u_reset_usb(netdev); */

// 	if(!err)
	//		err = p54u_reset_dev(netdev);
/* 	if(!err) */
/* 		err = p54u_load_firmware(netdev); */
/* 	if(!err) */
/* 		err = p54u_setup_dev(netdev); */
/*  	if(!err)  */
/* 	  err = p54u_mgmt_readback(netdev); */

/* 	if(!err)  */
/* 	  err = p54u_data_send(netdev); */


	release_firmware(p54u->fw_entry);
	
	p54u->running = 1;
	p54u->state = P54U_RUN;

	if(err) {
		p54u_err("%s: Boot the device failed: %i\n", netdev->name, err);
		return err;
	}
	
	p54u_info("%s: Boot the device done.\n", netdev->name);
	return 0;
}

int p54u_shutdown(struct net_device *netdev)
{
	struct p54u *p54u = netdev_priv(netdev);
	struct usb_device *usbdev = p54u->usbdev;
	u32 reg;

	p54u_dbg("%s: Shutting down the device.\n", netdev->name);

	/* so that the bottom halves wont resubmit urbs */
	p54u->state = P54U_SHUTDOWN;
	
	p54u_free_buffers(netdev);
	/* now there's no way work will be scheduled again, so we can wait on the bh's. Wait is sufficient for now*/
	p54u_mdelay(100);

	p54u->running = 0;

//	reg = p54u_brg_readl(netdev, P54U_BRG_CTRL_REG);
//	p54u_brg_writel(netdev, P54U_BRG_CTRL_REG, (reg | P54U_BRG_POWER_DOWN) & ~P54U_BRG_POWER_UP);
//	p54u_brg_writel(netdev, P54U_BRG_CTRL_REG, 0x3e);

	
	p54u_dbg("%s: Shutdown complete.\n", netdev->name);
	
	return 0;
}

static int p54u_validate_device(struct usb_interface *interface, const struct usb_device_id *id)
{
	/* See if the device offered us matches what we can accept */

	return 0;
}

static int p54u_probe(struct usb_interface *interface, const struct usb_device_id *id)
{
	struct usb_device *usbdev = interface_to_usbdev(interface);
	struct net_device *netdev;
	struct p54u *p54u;
	
	
	p54u_dbg("Prism54 USB Device Probe (Device number:%d): 0x%4.4x:0x%4.4x:0x%4.4x", usbdev->devnum, (int) usbdev->descriptor.idVendor, (int) usbdev->descriptor.idProduct, (int) usbdev->descriptor.bcdDevice);
	p54u_dbg("Device at %p", usbdev);
	p54u_dbg("Descriptor length: %x type: %x", (int) usbdev->descriptor.bLength, (int) usbdev->descriptor.bDescriptorType);
	
	if(p54u_validate_device(interface, id))
		return -ENODEV;
	
	netdev = alloc_etherdev(sizeof(*p54u));
	if (!netdev) {
		p54u_err("Failed to allocate netdevice.");
		return -ENOMEM;
	}
	
	SET_MODULE_OWNER(netdev);
	SET_NETDEV_DEV(netdev, &interface->dev);
	
	usb_set_intfdata(interface, netdev);
	
	p54u = netdev_priv(netdev);
	p54u->interface = interface;
	p54u->usbdev = usbdev;
	p54u->netdev = netdev;
	
	init_MUTEX (&p54u->disconnect_sem);
	memcpy(netdev->dev_addr, dummy_mac, ETH_ALEN);
	
	if(load_fw)
		/* Dont error if it fails, give it chance on open. */
		p54u_boot(netdev);

	if(p54u_setup_net(netdev)) {
		p54u_err("Failed to setup netdevice.");
		goto do_cleanup;
	}
	
	if(register_netdev(netdev)) {
		p54u_err("Failed to register netdevice.");
		goto do_cleanup;
	}
	
	p54u_info("Prism54 USB device now attached to %s", netdev->name);
	return 0;
	
    do_cleanup:
	p54u_free_buffers(netdev);
	usb_set_intfdata(interface, NULL);
	free_netdev(netdev);

	return -EIO;
}

static void p54u_disconnect(struct usb_interface *interface)
{
	struct net_device *netdev = usb_get_intfdata(interface);
	struct p54u *p54u = netdev_priv(netdev);
	struct usb_device *usbdev = p54u->usbdev;

	p54u_info("Prism54 USB device %s disconnecing\n", netdev->name);
	
	p54u_dbg("sem down\n");
	down(&p54u->disconnect_sem);
	p54u_dbg("sem down done\n");
	
	p54u->running = 0;
	
	p54u_dbg("Running = 0.\n");
	
	usb_set_intfdata(interface, NULL);
	
	p54u_dbg("Cleared interface data.\n");
	
	unregister_netdev(netdev);

	p54u_dbg("Unregistered netdevice.\n");
	
	p54u_free_buffers(netdev);
	
	p54u_dbg("Freed buffers.\n");
	
	free_netdev(netdev);
	
	p54u_dbg("Freed netdevice.\n");
		
	p54u_info("Disconnect complete.\n");

	p54u_dbg("sem up\n");
	up(&p54u->disconnect_sem);
	p54u_dbg("sem up done\n");
}

static struct usb_driver p54u_driver = {
	.owner = THIS_MODULE,
	.name = driver_name,
	.probe = p54u_probe,
	.disconnect = p54u_disconnect,
	.id_table = p54u_table,
};

static int __init p54u_init(void)
{
	int result;

	/* register this driver with the USB subsystem */
	result = usb_register(&p54u_driver);
	if (result) {
		p54u_err("usb_register failed. Error number %d", result);
		return result;
	}

	return 0;
}

static void __exit p54u_exit(void)
{
	/* deregister this driver with the USB subsystem */
	usb_deregister(&p54u_driver);
}

module_init(p54u_init);
module_exit(p54u_exit);

