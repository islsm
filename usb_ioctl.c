#include <linux/wireless.h>
#include <linux/netdevice.h> /* needed by iw_handler */
#include <net/iw_handler.h>

#include "ioctl.h"
#include "prism54_usb.h"
#include "channels.h"

#define SUPPORTED_WIRELESS_EXT                  16

void make_tx_control_channel(struct p54u *p54u, void *buf, int chan)
{
	struct p54u_mgmt_rx_frame *mgmt = &p54u->mgmt_frame;
	struct p54u_tx *tx = (struct p54u_tx *) buf;
	struct p54u_tx_control *txc = (struct p54u_tx_control *) &tx->data;
	struct p54u_tx_control_channel *tx_chan = (struct p54u_tx_control_channel *) &txc->data;
	int i;

	tx->magic1 =  P54U_TX_MAGIC1_CHANNEL;
	tx->length = cpu_to_le16(sizeof(*txc)-sizeof(void*)+sizeof(*tx_chan));
	txc->magic1 = cpu_to_le16(P54U_TX_CONTROL_NORESPONSE);
	txc->length = cpu_to_le16(sizeof(*tx_chan));
	/* numbered from 0 */
	chan = chan-1;
	

	tx_chan->magic1 = cpu_to_le32(P54U_TX_CONTROL_CHANNEL_MAGIC1_SCAN);
	tx_chan->freq = mgmt->l3[chan];
	tx_chan->unknown1 = cpu_to_le16(0x4808);
	/* maybe this is only a 32-bits value */
	tx_chan->l1data[0] = mgmt->l1[chan].data[0];
	tx_chan->l1data[1] = mgmt->l1[chan].data[1];
	tx_chan->l1data[2] = mgmt->l1[chan].data[2];
	tx_chan->l1data[3] = mgmt->l1[chan].data[3];

#define SUB(x,y) (u8)((x) - (y)) > (x) ? 0 : (x) - (y)
	for(i = 0; i < P54U_CHANNEL_DEFS; i++) {
		tx_chan->str[i].head=mgmt->l2[chan].data[i].head;
		tx_chan->str[i].tail=mgmt->l2[chan].data[i].tail;
		tx_chan->str[i].subtails[3]=SUB(tx_chan->str[i].tail,12);
		tx_chan->str[i].subtails[2]=SUB(tx_chan->str[i].subtails[3],12);
		tx_chan->str[i].subtails[1]=SUB(tx_chan->str[i].subtails[2],12);
		tx_chan->str[i].subtails[0]=SUB(tx_chan->str[i].subtails[1],14);
	}

	return;
}

/**** WARNING *****/
/* _ALL_ of these calls are bogus except for set_channel_freq */
/* I expect to complete statistics soon enough */

struct iw_statistics *
p54u_wireless_stats(struct net_device *ndev)
{
	struct p54u *priv = netdev_priv(ndev);

	/* completely bogus */
	return &priv->iwstatistics;
}


static int
prism54_commit(struct net_device *ndev, struct iw_request_info *info,
	       char *cwrq, char *extra)
{
	return 0;
}

static int
prism54_get_name(struct net_device *ndev, struct iw_request_info *info,
		 char *cwrq, char *extra)
{
	char *capabilities;
	int rvalue;

	capabilities = "IEEE 802.11b/g";	/* Default */
	
	strncpy(cwrq, capabilities, IFNAMSIZ);
	
	return 0;
}


static int
prism54_set_freq(struct net_device *netdev, struct iw_request_info *info,
		 struct iw_freq *fwrq, char *extra)
{
	struct p54u *p54u = netdev_priv(netdev);
	char* channel_packet = 0;
	int c;
	
	channel_packet=kmalloc(FULL_TX_CONTROL_CHANNEL,GFP_KERNEL);
	if (!channel_packet)
		return -ENOMEM;

	memset(channel_packet,0,FULL_TX_CONTROL_CHANNEL);

	if (fwrq->m < 1000 && fwrq->m >= 1 && fwrq->m < P54U_NR_CHANNELS)
		/* we have a valid channel number */
		c = fwrq->m;
	else
		/* we dont handle freq_to_channel yet */
		return -ENOTSUPP;

	make_tx_control_channel(p54u, channel_packet, c);
	p54u_control_msg(netdev, p54u_freqs[c-1], FULL_TX_CONTROL_CHANNEL);
	p54u_wait_data(netdev);

	kfree(channel_packet);
	
	return p54u->err;
}

static int
prism54_get_freq(struct net_device *ndev, struct iw_request_info *info,
		 struct iw_freq *fwrq, char *extra)
{
	fwrq->m = 272;
	fwrq->e = 0;

	return 0;
}

static int
prism54_set_mode(struct net_device *ndev, struct iw_request_info *info,
		 __u32 * uwrq, char *extra)
{
/* 	islpci_private *priv = netdev_priv(ndev); */
/* 	u32 mlmeautolevel = CARD_DEFAULT_MLME_MODE; */

/* 	/\* Let's see if the user passed a valid Linux Wireless mode *\/ */
/* 	if (*uwrq > IW_MODE_MONITOR || *uwrq < IW_MODE_AUTO) { */
/* 		printk(KERN_DEBUG */
/* 		       "%s: %s() You passed a non-valid init_mode.\n", */
/* 		       priv->ndev->name, __FUNCTION__); */
/* 		return -EINVAL; */
/* 	} */

/* 	down_write(&priv->mib_sem); */

/* 	if (prism54_mib_mode_helper(priv, *uwrq)) { */
/* 		up_write(&priv->mib_sem); */
/* 		return -EOPNOTSUPP; */
/* 	} */

/* 	/\* the ACL code needs an intermediate mlmeautolevel. The wpa stuff an */
/* 	 * extended one. */
/* 	 *\/ */
/* 	if ((*uwrq == IW_MODE_MASTER) && (priv->acl.policy != MAC_POLICY_OPEN)) */
/* 		mlmeautolevel = DOT11_MLME_INTERMEDIATE; */
/* 	if (priv->wpa) */
/* 		mlmeautolevel = DOT11_MLME_EXTENDED; */

/* 	mgt_set(priv, DOT11_OID_MLMEAUTOLEVEL, &mlmeautolevel); */

/* 	mgt_commit(priv); */
/* 	priv->ndev->type = (priv->iw_mode == IW_MODE_MONITOR) */
/* 	    ? priv->monitor_type : ARPHRD_ETHER; */
/* 	up_write(&priv->mib_sem); */

	return 0;
}

/* Use mib cache */
static int
prism54_get_mode(struct net_device *ndev, struct iw_request_info *info,
		 __u32 * uwrq, char *extra)
{
/* 	islpci_private *priv = netdev_priv(ndev); */

/* 	BUG_ON((priv->iw_mode < IW_MODE_AUTO) || (priv->iw_mode > */
/* 						  IW_MODE_MONITOR)); */
	*uwrq = IW_MODE_INFRA;

	return 0;
}

/* we use DOT11_OID_EDTHRESHOLD. From what I guess the card will not try to
 * emit data if (sensitivity > rssi - noise) (in dBm).
 * prism54_set_sens does not seem to work.
 */

static int
prism54_set_sens(struct net_device *ndev, struct iw_request_info *info,
		 struct iw_param *vwrq, char *extra)
{
/* 	islpci_private *priv = netdev_priv(ndev); */
/* 	u32 sens; */

/* 	/\* by default  the card sets this to 20. *\/ */
/* 	sens = vwrq->disabled ? 20 : vwrq->value; */

/* 	return mgt_set_request(priv, DOT11_OID_EDTHRESHOLD, 0, &sens); */
	return 0;
}

static int
prism54_get_sens(struct net_device *ndev, struct iw_request_info *info,
		 struct iw_param *vwrq, char *extra)
{
/* 	islpci_private *priv = netdev_priv(ndev); */
/* 	union oid_res_t r; */
/* 	int rvalue; */

/* 	rvalue = mgt_get_request(priv, DOT11_OID_EDTHRESHOLD, 0, NULL, &r); */

	vwrq->value = 0;
	vwrq->disabled = (vwrq->value == 0);
	vwrq->fixed = 1;

	return 0;
}

static int
prism54_get_range(struct net_device *ndev, struct iw_request_info *info,
		  struct iw_point *dwrq, char *extra)
{
	struct iw_range *range = (struct iw_range *) extra;
	char *data;
	int i, m, rvalue;

	memset(range, 0, sizeof (struct iw_range));
	dwrq->length = sizeof (struct iw_range);

	/* set the wireless extension version number */
	range->we_version_source = SUPPORTED_WIRELESS_EXT;
	range->we_version_compiled = WIRELESS_EXT;

	/* Now the encoding capabilities */
	range->num_encoding_sizes = 3;
	/* 64(40) bits WEP */
	range->encoding_size[0] = 5;
	/* 128(104) bits WEP */
	range->encoding_size[1] = 13;
	/* 256 bits for WPA-PSK */
	range->encoding_size[2] = 32;
	/* 4 keys are allowed */
	range->max_encoding_tokens = 4;

	/* we don't know the quality range... */
	range->max_qual.level = 0;
	range->max_qual.noise = 0;
	range->max_qual.qual = 0;
	/* these value describe an average quality. Needs more tweaking... */
	range->avg_qual.level = -80;	/* -80 dBm */
	range->avg_qual.noise = 0;	/* don't know what to put here */
	range->avg_qual.qual = 0;

	range->sensitivity = 200;

	/* retry limit capabilities */
	range->retry_capa = IW_RETRY_LIMIT | IW_RETRY_LIFETIME;
	range->retry_flags = IW_RETRY_LIMIT;
	range->r_time_flags = IW_RETRY_LIFETIME;

	/* I don't know the range. Put stupid things here */
	range->min_retry = 1;
	range->max_retry = 65535;
	range->min_r_time = 1024;
	range->max_r_time = 65535 * 1024;

	/* txpower is supported in dBm's */
	range->txpower_capa = IW_TXPOW_DBM;

	return 0;
}

/* Set AP address*/

static int
prism54_set_wap(struct net_device *ndev, struct iw_request_info *info,
		struct sockaddr *awrq, char *extra)
{
/* 	islpci_private *priv = netdev_priv(ndev); */
/* 	char bssid[6]; */
/* 	int rvalue; */

/* 	if (awrq->sa_family != ARPHRD_ETHER) */
/* 		return -EINVAL; */

/* 	/\* prepare the structure for the set object *\/ */
/* 	memcpy(&bssid[0], awrq->sa_data, 6); */

/* 	/\* set the bssid -- does this make sense when in AP mode? *\/ */
/* 	rvalue = mgt_set_request(priv, DOT11_OID_BSSID, 0, &bssid); */

	return -EINPROGRESS;	/* Call commit handler */
}

/* get AP address*/

static int
prism54_get_wap(struct net_device *ndev, struct iw_request_info *info,
		struct sockaddr *awrq, char *extra)
{
	int rvalue;

	rvalue = 0;
//	awrq->sa_family = ARPHRD_ETHER;

	return rvalue;
}

static int
prism54_set_scan(struct net_device *dev, struct iw_request_info *info,
		 struct iw_param *vwrq, char *extra)
{
	/* hehe the device does this automagicaly */
	return 0;
}


static const iw_handler prism54_handler[] = {
	(iw_handler) prism54_commit,	/* SIOCSIWCOMMIT */
	(iw_handler) prism54_get_name,	/* SIOCGIWNAME */
	(iw_handler) NULL,	/* SIOCSIWNWID */
	(iw_handler) NULL,	/* SIOCGIWNWID */
	(iw_handler) prism54_set_freq,	/* SIOCSIWFREQ */
	(iw_handler) prism54_get_freq,	/* SIOCGIWFREQ */
	(iw_handler) prism54_set_mode,	/* SIOCSIWMODE */
	(iw_handler) prism54_get_mode,	/* SIOCGIWMODE */
	(iw_handler) prism54_set_sens,	/* SIOCSIWSENS */
	(iw_handler) prism54_get_sens,	/* SIOCGIWSENS */
	(iw_handler) NULL,	/* SIOCSIWRANGE */
	(iw_handler) prism54_get_range,	/* SIOCGIWRANGE */
	(iw_handler) NULL,	/* SIOCSIWPRIV */
	(iw_handler) NULL,	/* SIOCGIWPRIV */
	(iw_handler) NULL,	/* SIOCSIWSTATS */
	(iw_handler) NULL,	/* SIOCGIWSTATS */
	(iw_handler) NULL,	//	prism54_set_spy,	/* SIOCSIWSPY */
	(iw_handler) NULL,	//	iw_handler_get_spy,	/* SIOCGIWSPY */
	(iw_handler) NULL,	//	iw_handler_set_thrspy,	/* SIOCSIWTHRSPY */
	(iw_handler) NULL,	//	iw_handler_get_thrspy,	/* SIOCGIWTHRSPY */
	(iw_handler) prism54_set_wap,	/* SIOCSIWAP */
	(iw_handler) prism54_get_wap,	/* SIOCGIWAP */
	(iw_handler) NULL,	/* -- hole -- */
	(iw_handler) NULL,	/* SIOCGIWAPLIST depreciated */
	(iw_handler) NULL,	//	(iw_handler) prism54_set_scan,	/* SIOCSIWSCAN */
	(iw_handler) NULL,	//	(iw_handler) prism54_get_scan,	/* SIOCGIWSCAN */
	(iw_handler) NULL,	//	(iw_handler) prism54_set_essid,	/* SIOCSIWESSID */
	(iw_handler) NULL,	//	(iw_handler) prism54_get_essid,	/* SIOCGIWESSID */
	(iw_handler) NULL,	//	(iw_handler) prism54_set_nick,	/* SIOCSIWNICKN */
	(iw_handler) NULL,	//	(iw_handler) prism54_get_nick,	/* SIOCGIWNICKN */
	(iw_handler) NULL,	/* -- hole -- */
	(iw_handler) NULL,	/* -- hole -- */
	(iw_handler) NULL,	//	(iw_handler) prism54_set_rate,	/* SIOCSIWRATE */
	(iw_handler) NULL,	//	(iw_handler) prism54_get_rate,	/* SIOCGIWRATE */
	(iw_handler) NULL,	//	(iw_handler) prism54_set_rts,	/* SIOCSIWRTS */
	(iw_handler) NULL,	//	(iw_handler) prism54_get_rts,	/* SIOCGIWRTS */
	(iw_handler) NULL,	//	(iw_handler) prism54_set_frag,	/* SIOCSIWFRAG */
	(iw_handler) NULL,	//	(iw_handler) prism54_get_frag,	/* SIOCGIWFRAG */
	(iw_handler) NULL,	//	(iw_handler) prism54_set_txpower,	/* SIOCSIWTXPOW */
	(iw_handler) NULL,	//	(iw_handler) prism54_get_txpower,	/* SIOCGIWTXPOW */
	(iw_handler) NULL,	//	(iw_handler) prism54_set_retry,	/* SIOCSIWRETRY */
	(iw_handler) NULL,	//	(iw_handler) prism54_get_retry,	/* SIOCGIWRETRY */
	(iw_handler) NULL,	//	(iw_handler) prism54_set_encode,	/* SIOCSIWENCODE */
	(iw_handler) NULL,	//	(iw_handler) prism54_get_encode,	/* SIOCGIWENCODE */
	(iw_handler) NULL,	/* SIOCSIWPOWER */
	(iw_handler) NULL,	/* SIOCGIWPOWER */
};

static const struct iw_priv_args prism54_private_args[] = {};

static const iw_handler prism54_private_handler[] = {};

const struct iw_handler_def p54u_handler_def = {
	.num_standard = sizeof (prism54_handler) / sizeof (iw_handler),
	.num_private = sizeof (prism54_private_handler) / sizeof (iw_handler),
	.num_private_args =
	    sizeof (prism54_private_args) / sizeof (struct iw_priv_args),
	.standard = (iw_handler *) prism54_handler,
	.private = (iw_handler *) prism54_private_handler,
	.private_args = (struct iw_priv_args *) prism54_private_args,
	.spy_offset = 0,//offsetof(islpci_private, spy_data),
};
