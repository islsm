/*
 * Prism54 USB driver
 */

#include <linux/config.h>
#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/module.h>
#include <linux/smp_lock.h>
#include <linux/completion.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/wireless.h>
#include <asm/uaccess.h>
#include <linux/usb.h>

#include "prism54_usb.h"
#include "isl_38xx.h"
#include "ioctl.h"

const char dummy_mac[ETH_ALEN] = {0x00, 0x3d, 0xb4, 0x00, 0x00, 0x00};

static int p54u_open(struct net_device *netdev)
{
	struct p54u *p54u = netdev_priv(netdev);
	struct usb_device *usbdev = p54u->usbdev;
	
	p54u_info("sem down\n");
	down (&p54u->disconnect_sem);
	p54u_info("sem down done\n");

	
	if(!p54u->running)
	{
		p54u_info("Not running on open, booting\n");
		if(p54u_boot(netdev)) {
			p54u_err("Failed to boot device.\n");
			goto do_err;
		}
	}
	
	netif_start_queue(netdev);
	p54u_info("sem up\n");
	up(&p54u->disconnect_sem);
	p54u_info("sem up done\n");
	return 0;

    do_err:
    	p54u_err("Failed to open device.\n");
	p54u_shutdown(netdev);
	p54u_info("sem up\n");
	up(&p54u->disconnect_sem);
	p54u_info("sem up done\n");
	return -EIO;
}

static int p54u_close(struct net_device *netdev)
{
	struct p54u *p54u = netdev_priv(netdev);
	struct usb_device *usbdev = p54u->usbdev;
	
	netif_stop_queue(netdev);

	if(!load_fw)
	{
		p54u_info("Close with firmware unload\n");
		if (p54u->running)
		{
			p54u_info("Running on close,shutting down\n");
			if (p54u_shutdown(netdev))
			{
				p54u_info("Shutdown failed\n");
				return -EIO;
			}
		}
	}	
	
	return 0;
}

static void p54u_tx_timeout(struct net_device *netdev)
{
	struct p54u *p54u = netdev_priv(netdev);
	struct usb_device *usbdev = p54u->usbdev;
	
	return;
}

static int p54u_transmit(struct sk_buff *skb, struct net_device *netdev)
{
	struct p54u *p54u = netdev_priv(netdev);
	struct usb_device *usbdev = p54u->usbdev;

	p54u_info("xmit\n");
	netdev->trans_start = jiffies;
	/* do the submit urb stuff */
	dev_kfree_skb(skb);
	
	return 0;
}

static struct net_device_stats *
p54u_statistics(struct net_device *netdev)
{
	struct p54u *p54u = netdev_priv(netdev);
	
	return &p54u->statistics;
}

static int 
p54u_ioctl(struct net_device *ndev, struct ifreq *rq, int cmd)
{
	return -EOPNOTSUPP;
}

int p54u_setup_net(struct net_device *netdev)
{
	struct p54u *p54u = netdev_priv(netdev);
	struct usb_device *usbdev = p54u->usbdev;

	/* initialize the function pointers */
	netdev->open = &p54u_open;
	netdev->stop = &p54u_close;
	netdev->get_stats = &p54u_statistics;
	netdev->get_wireless_stats = &p54u_wireless_stats;
	netdev->do_ioctl = &p54u_ioctl;
	netdev->wireless_handlers = (struct iw_handler_def *) &p54u_handler_def;

	netdev->hard_start_xmit = &p54u_transmit;
	netdev->addr_len = ETH_ALEN;
//	netdev->set_mac_address = &prism54_set_mac_address;

	netdev->watchdog_timeo = P54U_TX_TIMEOUT;
	netdev->tx_timeout = &p54u_tx_timeout;

	return 0;
}

