/*
 * Prism54 USB driver
 */

#include <linux/config.h>
#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/module.h>
#include <linux/smp_lock.h>
#include <linux/completion.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/usb.h>
#include <linux/pci.h>
#include <linux/firmware.h>
#include <asm/uaccess.h>
#include "prism54_usb.h"
#include "isl_38xx.h"

void p54u_mdelay(int ms)
{
	int t = (ms * HZ + 500) / 1000;
	
	if(!t && ms)
		t = 1;
	
	set_current_state(TASK_UNINTERRUPTIBLE);
	while(t) {
		//printk("t: %i\n", t);
		t = schedule_timeout(t);
	}
	
	return;
}


void p54u_data_debug(struct net_device *netdev, unsigned int ep, void *_data, int len)
{
	struct p54u *p54u = netdev_priv(netdev);
	struct usb_device *usbdev = p54u->usbdev;
	unsigned char *data = _data;
	int k, l, in;
	unsigned long int time = jiffies;
	
	in = ep & USB_DIR_IN;
	if(debug > 1) {
		for(k = 0; k < len; k+= 16) {
			if(k == 0) {
				printk(KERN_CRIT "[%s >%-6lld<%-6lld+%-6ld]     %02x %s 00000000:", in?"IN ":"OUT", 0LL, 0LL, time, ep, in?"<-":"->");
			} else {
				printk(KERN_CRIT "                                   -> %08x:", k);
			}
			for (l = 0; (l < 16) && (k + l < len); ++l) {
				printk(" %02x", ((unsigned char *)data)[k + l]);
			}
			printk("\n");
		}
	}
}

int p54u_bulk_msg(struct net_device *netdev, unsigned int ep, void *_data, int len)
{
	struct p54u *p54u = netdev_priv(netdev);
	struct usb_device *usbdev = p54u->usbdev;
	unsigned char *data = _data;
	size_t alen;
	int pipe, err, k, l;

	p54u_data_debug(netdev, ep, data, len);
	
	pipe = usb_sndbulkpipe(usbdev, ep);
	err = usb_bulk_msg(usbdev, pipe, data, len, &alen, 2 * HZ);
	if(err)
		p54u_info("bulk submit failed: %i\n", err);
	
	return err;
}


u32 p54u_reg_rw(struct net_device *netdev, int write, int ep, int port, u32 addr, u32 val)
{
	struct p54u *p54u = netdev_priv(netdev);
	struct usb_device *usbdev = p54u->usbdev;
	struct p54u_reg reg;
	unsigned int pipe;
	size_t len,retlen;
	int err, i, epd;
	u64 t, t1, t2;
	
	reg.port = cpu_to_le16(port);
	reg.addr = cpu_to_le32(addr);
	reg.val = ((port & P54U_PORT_U32) == P54U_PORT_U32) ? cpu_to_le32(val) : cpu_to_le32(val&0xffff);
	len = write ? P54U_REG_WSIZE : P54U_REG_RSIZE;
	pipe = usb_sndbulkpipe(usbdev, ep & USB_ENDPOINT_NUMBER_MASK);
	
	t1 = p54u_time();
	err = usb_bulk_msg(usbdev, pipe, &reg, len, &retlen, HZ);
	t2 = p54u_time();
	t = t2 - t1;
	
	p54u_data_debug(netdev, ep, &reg, len);

	if(err) {
		p54u_err("%s %02x %04x %08x %08x: failed: %i", write ? "Write": "Read", ep, port, addr, val, err);
		//p54u->err = err;
		if(write && (ep == P54U_PIPE_DEV ) && (len != retlen)) {
			/* something bad happened, but we know how to cope with this case. We should do it for reads, too */
			p54u_err("expected to write %i but wrote %i on dev pipe, resetting",len,retlen);
			/* clear stall */
			usb_clear_halt(usbdev,pipe);
			/* resubmit only once */
			err = usb_bulk_msg(usbdev, pipe, &reg, len, &retlen, HZ);
			if(err) {
				p54u_err("%s %02x %04x %08x %08x: failed: %i", write ? "Write": "Read", ep, port, addr, val, err);
				p54u->err = err;
			}
		}
		else {
			p54u->err = err;
		}
		return 0;
	}
	
	if(write) {
//		udelay(ISL38XX_WRITEIO_DELAY);
		return 0;
	}
	
	pipe = usb_rcvbulkpipe(usbdev, ep & USB_ENDPOINT_NUMBER_MASK);
	len = sizeof(reg.val);
	
	t1 = p54u_time();
	err = usb_bulk_msg(usbdev, pipe, &reg.val, len, &len, HZ);
	t2 = p54u_time();
	t = t2 - t1;
	
	if(err) {
		p54u_err("Register read %02x %04x %08x %08x: failed: %i", ep, port, addr, val, err);
		p54u->err = err;
		return 0;
	}
	
	p54u_data_debug(netdev, ep | USB_DIR_IN, &reg.val, len);
	
	return ((port & P54U_PORT_U32) == P54U_PORT_U32) ? le32_to_cpu(reg.val) : (le32_to_cpu(reg.val)&0xffff);
}

static void p54u_wait_ctrl(struct urb *urb, struct pt_regs *regs) {
  struct completion *comp = (struct completion *) urb->context;
  complete(comp);
  return;
}

int p54u_annouced_msg(struct net_device *netdev, unsigned int pipe, void *data, int data_len) 
{
  struct p54u *p54u = netdev_priv(netdev);
  struct usb_device *usbdev = p54u->usbdev;
  struct p54u_reg *reg = 0;
  struct urb *urb = 0;
  unsigned int ctrlpipe;
  struct completion done;
  void *remap_buf = 0;

  size_t retlen;
  int err, i;
  int val = 0;
  int remapped = 0;

  init_completion(&done);
  
  reg = (struct p54u_reg *) kmalloc(sizeof(struct p54u_reg),GFP_KERNEL);
  if (!reg)
    return -ENOMEM;

  urb = usb_alloc_urb(0,GFP_KERNEL);
  if (!urb)
  {
	  kfree(reg);
	  return -ENOMEM;
  }

  ctrlpipe = usb_sndbulkpipe(usbdev, P54U_PIPE_DEV & USB_ENDPOINT_NUMBER_MASK);

  usb_fill_bulk_urb(urb, usbdev, ctrlpipe, (char *) reg, P54U_REG_WSIZE, 
		    p54u_wait_ctrl, &done);
  
  if (pipe == P54U_PIPE_DATA)
    {
      val = ISL38XX_DEV_INT_DATA;
    }
  else if (pipe == P54U_PIPE_MGMT)
    {
      val = ISL38XX_DEV_INT_MGMT;
    }

  reg->port = cpu_to_le16(P54U_PORT_DEV_U32);
  reg->addr = cpu_to_le32(P54U_DEV_BASE | ISL38XX_DEV_INT_REG);
  reg->val = ((P54U_PORT_DEV_U32 & P54U_PORT_U32) == P54U_PORT_U32) ? cpu_to_le32(val) : cpu_to_le32(val&0xffff);

  /* remap data buffer if non-dmable */
  //  if (!virt_addr_valid(remap_buf)) {

  remapped = 1;
  remap_buf = kmalloc(data_len, GFP_KERNEL);
  if (!remap_buf)
    return -ENOMEM;
  memcpy(remap_buf, data, data_len);

  /* Send first one asynchronously and wait for last one */
  p54u->err = usb_submit_urb(urb, GFP_KERNEL);
  if (p54u->err == 0)
    {
      p54u_data_debug(netdev, P54U_PIPE_DEV, reg, P54U_REG_WSIZE );
      p54u->err = p54u_bulk_msg(netdev, pipe, remap_buf,data_len);
      wait_for_completion(&done);
    }

  if (remapped)
	  kfree(remap_buf);
  usb_free_urb(urb);
  kfree(reg);
  
  return p54u->err;
}


static void timeout_kill(unsigned long data) {
  struct p54u_pipe *pipe = (struct p54u_pipe *) data;
  struct completion *comp = &pipe->comp;
  //  p54u->err=-ETIMEDOUT; we can put the value back in the pipe
  complete(comp);
  printk("timeout waiting");
  return;
}

static int p54u_wait_timeout(struct p54u_pipe *pipe, unsigned int delay)
{
	struct timer_list timer;
	
	init_timer(&timer);
	timer.expires = jiffies + HZ*delay;
	timer.data = (unsigned long)(pipe);
	timer.function = timeout_kill;

	add_timer(&timer);
	wait_for_completion(&pipe->comp);
	del_timer_sync(&timer);

	return 0;
}

int p54u_wait_int(struct net_device *netdev)
{
	struct p54u *p54u = netdev_priv(netdev);
	struct usb_device *usbdev = p54u->usbdev;

	p54u_wait_timeout(&p54u->int_rx,1);

	return p54u->pending;
}

int p54u_wait_mgmt_response(struct net_device *netdev)
{
	struct p54u *p54u = netdev_priv(netdev);
	struct usb_device *usbdev = p54u->usbdev;

	p54u_wait_timeout(&p54u->mgmt_rx,1);

	return p54u->pending;
}

int p54u_wait_data(struct net_device *netdev)
{
	struct p54u *p54u = netdev_priv(netdev);
	struct usb_device *usbdev = p54u->usbdev;

	p54u_wait_timeout(&p54u->data_rx,1);

	return p54u->pending;
}

void p54u_int_ack(void *data) 
{
	struct net_device *netdev = (struct net_device *) data;
	struct p54u *p54u = netdev_priv(netdev);
	u32 pending = p54u->pending;
	u32 reg;
	
	/* disable interrupt in ISL */
	p54u_dev_writel(netdev, ISL38XX_INT_EN_REG, 0);

	do {
		/* ack the interrupts */
		reg = p54u_dev_readl(netdev, ISL38XX_INT_IDENT_REG);
		p54u_info("isl int vector: %08x\n", reg);
		if (reg & ~(0x80000000))
			p54u_dev_writel(netdev, ISL38XX_INT_ACK_REG, reg);
	}
	while(reg & ~(0x80000000));
	
	/* clear the interrupt at net2280 level
	   the net2280 is in host mode */
	p54u_brg_writel(netdev, NET2280_IRQSTAT1, NET2280_PCI_INTA_INTERRUPT);
	
	/* reenable interrupts in ISL */
	p54u_dev_writel(netdev, ISL38XX_INT_EN_REG, 0x00004004);

	/* resubmit URB, only if we're not stopping */
	if (p54u->state != P54U_SHUTDOWN)
	  { 
	    p54u->err = usb_submit_urb(p54u->int_rx.urb[0], GFP_KERNEL);
	    if(p54u->err) {
	      p54u_dbg("%s: Error submit int 0: %i\n", netdev->name, p54u->err);
	    }
	  }
}


void p54u_int_rx_cb(struct urb *urb, struct pt_regs *p)
{
	struct net_device *netdev = urb->context;
	struct p54u *p54u = netdev_priv(netdev);
	struct usb_device *usbdev = p54u->usbdev;
	int err, pending;
	u32 reg;
	u64 t;
	
	if (urb->status != 0)
	  {
	    p54u_dbg("interrupt handler abnormal termination");
	    /* This is bound to happen upon urb handback */
	    return;
	  }
	
	pending = *(int *)urb->transfer_buffer;
	p54u_data_debug(netdev, 0x8f, urb->transfer_buffer, urb->actual_length);
	p54u->pending = le32_to_cpu(pending);
	if(p54u->pending & NET2280_PCI_INTA_INTERRUPT)
		p54u_dbg("interrupt from prism chip");
	
	if(p54u->pending & ~NET2280_PCI_INTA_INTERRUPT)
		p54u_dbg("interrupt from unknown source");
	
	// only complete on return to zero
	if(p54u->pending == 0)
		{
			p54u_dbg("interrupt returning to inactive, scheduling work");
			schedule_work(&p54u->int_ack);
			complete(&p54u->int_rx.comp);
		} else {
			p54u_dbg("interrupts pending, scheduling work");
			schedule_work(&p54u->int_ack);
		}

	// in the end only complete when == 0;
	
	return;
}

void p54u_data_rx_cb(struct urb *urb, struct pt_regs *p)
{
	struct net_device *netdev = urb->context;
	struct p54u *p54u = netdev_priv(netdev);
	struct usb_device *usbdev = p54u->usbdev;
	int err;

	if (urb->status != 0)
	  {
	    p54u_dbg("data rx abnormal termination");
	    return;
	  }

	atomic_dec(&p54u->data_rx.busy);
	complete(&p54u->data_rx.comp);

	p54u_dbg("data RX");
	/* this will resubmit the urb */
	schedule_work(&p54u->data_bh);
	return;
}

void p54u_data_rx(void *data)
{
  struct net_device *netdev = (struct net_device *) data;
  struct p54u *p54u = netdev_priv(netdev);
  
  /* for now will be the only thing we do with it : display */

  p54u_data_debug(netdev, 0x81, p54u->data_rx.buf[0], p54u->data_rx.urb[0]->actual_length);
  p54u_data_rx_submit(netdev);
}

void p54u_mgmt_rx_cb(struct urb *urb, struct pt_regs *p)
{
	struct net_device *netdev = urb->context;
	struct p54u *p54u = netdev_priv(netdev);
	struct usb_device *usbdev = p54u->usbdev;

	if (urb->status != 0)
	  {
	    p54u_dbg("mgmt rx abnormal termination");
	    return;
	  }
	
	atomic_dec(&p54u->mgmt_rx.busy);
	complete(&p54u->mgmt_rx.comp);
	p54u_data_debug(netdev, 0x82, urb->transfer_buffer, urb->actual_length);
	return;
}

int p54u_mgmt_rx_submit(struct net_device *netdev)
{
	int err = 0;
	struct p54u *p54u = netdev_priv(netdev);
	if (p54u->state != P54U_SHUTDOWN)
	  { 
	    err = usb_submit_urb(p54u->mgmt_rx.urb[0], GFP_KERNEL);
	    if(err) {
	      p54u_info("mgmt submit failed  %i\n", err);
	    }
	    else {
	      atomic_inc(&p54u->mgmt_rx.busy);
	      p54u_dbg("%s: Submit mgmt ok.\n", netdev->name);
	    }
	    return err;
	  }
	return -1;
}

int p54u_data_rx_submit(struct net_device *netdev)
{
	int err = 0;
	struct p54u *p54u = netdev_priv(netdev);

	if (p54u->state != P54U_SHUTDOWN)
	  { 
	    
	    err = usb_submit_urb(p54u->data_rx.urb[0], GFP_KERNEL);
	    if(err) {
	      p54u_info("data submit failed  %i\n", err);
	    }
	    else {
	      atomic_inc(&p54u->data_rx.busy);
	      p54u_dbg("%s: Submit data ok.\n", netdev->name);
	    }
	    return err;
	  }
	return -1;
}
